import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Book } from '../../models/book.model';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, OnDestroy {
  books: Book[] = []
  booksSubscription: Subscription;

  constructor(private bookService: BooksService, private router: Router) { }

  ngOnInit(): void {
    this.booksSubscription = this.bookService.booksSubject.subscribe((books: Book[]) => {
      this.books = books
    })
    this.bookService.emitBooks()
  }

  onNewBook(){
    this.router.navigate(['/book', 'new'])
  }

  onDeleteBook(book: Book){
    this.bookService.removeBook(book)
  }

  onViewBook(id: number){
    console.log(id);
    this.router.navigate(['/book', 'view', id])
  }

  ngOnDestroy(): void{
    this.booksSubscription.unsubscribe()
  }

}
