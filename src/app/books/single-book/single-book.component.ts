import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Book } from '../../models/book.model';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.css'],
})
export class SingleBookComponent implements OnInit {
  book: Book = new Book('', '');
  constructor(
    private route:  ActivatedRoute,
    private bookService: BooksService,
    private router: Router
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['id']
    this.bookService.getSingleBook(+id).then((book: any) => {
      this.book.title = book.title
      this.book.author = book.author
      this.book.photo = book.photo
    })
  }

  onBack(){
    this.router.navigate(['/books'])
  }
}
