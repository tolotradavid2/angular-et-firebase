import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Book } from '../../models/book.model';
import { BooksService } from '../../services/books.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css'],
})
export class BookFormComponent implements OnInit {
  bookForm: FormGroup;
  fileIsUploading: boolean = false;
  fileUrl: string;
  fileUploaded: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    private bookService: BooksService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.bookForm = this.formBuilder.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      synopsis: '',
    });
  }

  onSaveBook() {
    const title: string = this.bookForm.get('title')?.value.toLowerCase();
    const author = this.bookForm.get('author')?.value.toLowerCase();
    const synopsis = this.bookForm.get('synopsis')?.value;
    const newBook = new Book(title, author);
    newBook.synopsis = synopsis;
    if (this.fileUrl && this.fileUrl !== '') {
      newBook.photo = this.fileUrl;
    }
    this.bookService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  detectFiles(event: any){
    this.onUploadFile(event.target?.files[0])
  }

  onUploadFile(file: File) {
    this.fileIsUploading = true;
    this.bookService.uploadFile(file).then((url: any) => {
      this.fileUrl = url.toString();
      this.fileIsUploading = false;
      this.fileUploaded = true;
    });
  }

  onGoBack(){
    this.router.navigate(['/books'])
  }
}
