import { Component } from '@angular/core';
import firebase from 'firebase/app'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(){
    const config = {
      apiKey: "AIzaSyAsyG3MDtqGlvy1lUCapn1smmg7EDnvKo0",
      authDomain: "gestionnaire-de-livres.firebaseapp.com",
      projectId: "gestionnaire-de-livres",
      storageBucket: "gs://gestionnaire-de-livres.appspot.com/",
      databaseUrl: "https://gestionnaire-de-livres-default-rtdb.firebaseio.com/",
      messagingSenderId: "131131665792",
      appId: "1:131131665792:web:8459406366525fba976565"
    };
    firebase.initializeApp(config)
  }
}
