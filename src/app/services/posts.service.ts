import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Subject } from 'rxjs';
import { Post } from '../models/post.model';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root',
})
export class PostsService {
  private posts: Post[] = [];
  postsSubject = new Subject<Post[]>();

  constructor() {
    this.getPosts();
  }

  emitPosts(): void {
    this.postsSubject.next(this.posts.slice());
  }

  getPosts() {
    firebase
      .database()
      .ref('/posts')
      .on('value', (data: DataSnapshot) => {
        this.posts = data.val() ? data.val() : [];
        this.emitPosts();
      });
  }

  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
  }

  createNewPost(post: Post): void {
    const newPost = post;
    newPost.loveIts = 0;
    newPost.dontLoveIts = 0;
    this.posts.push(newPost);
    this.savePosts();
    this.emitPosts();
  }

  update(index: number, post: Post): void {
    this.posts[index] = post;
    this.savePosts();
    this.emitPosts();
  }

  removePost(post: Post) {
    const postIndex = this.posts.findIndex((el) => el === post);
    this.posts.slice(postIndex, 1);
    this.savePosts();
    this.emitPosts();
  }
}
