import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css'],
})
export class PostListItemComponent implements OnInit {
  @Input()
  title: string;
  @Input()
  content: string;
  @Input()
  loveIts: number;
  @Input()
  dontLoveIts: number;
  @Input()
  index: number;

  private post = new Post;
  constructor(private postsService: PostsService) {}

  ngOnInit(): void {}

  onLove() {
    this.loveIts++;
    this.postsService.update(this.index, this.postUpdated);
  }

  onDontLove() {
    this.dontLoveIts++;
    this.postsService.update(this.index, this.postUpdated);
  }

  get postUpdated(): Post {
    this.post.title = this.title
    this.post.content = this.content
    this.post.loveIts = this.loveIts
    this.post.dontLoveIts = this.dontLoveIts
    return this.post
  }
}
