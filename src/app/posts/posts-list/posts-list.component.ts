import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit, OnDestroy {
  posts: Post[]
  postsSubscription: Subscription
  constructor(private postsService: PostsService, private router: Router) { }

  ngOnInit(): void {
    this.postsSubscription = this.postsService.postsSubject.subscribe((data: Post[]) => {
      this.posts = data
    })
    this.postsService.emitPosts()
  }

  onAddNewPost(){
    this.router.navigate(['/post', 'new'])
  }

  ngOnDestroy(): void {
    this.postsSubscription.unsubscribe()
  }
}
