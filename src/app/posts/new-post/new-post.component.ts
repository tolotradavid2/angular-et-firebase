import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { PostsService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css'],
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;
  private newPost = new Post();
  constructor(
    private postsService: PostsService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.postForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      content: ['', [Validators.required]],
    });
  }

  onSave(): void {
    this.newPost.title = this.postForm.get('title')?.value;
    this.newPost.content = this.postForm.get('content')?.value;
    this.postsService.createNewPost(this.newPost);
    this.router.navigate(['/posts']);
  }

  onCancel() {
    this.router.navigate(['/posts']);
  }
}
